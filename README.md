# README

This is the README file for CodePolitiks future API. CodePolitiks is a proyect in development where technology and political discussion converge. CodePolitiks is all about understanding and discussing
about the importance of technology in the political activity, and vice versa.

This is a RESTful API with devise token authentication. It's development has and will always be test driven, and in a certain way, it was just an excuse to try the new awesome Rails 5 features.

Things you may want to know:

* Ruby Version: 2.3.1p112

* Rails Version: 5.0.0.1

* To start API simply `bundle install` and `rails s`

* Currently using SQLite DB, in production should be PostGres

* To initialize DB run `rails db:migrate` `rails db:seed`

* To run test you can use guard. Runing `bundle exec guard rspec`. This will not only run your tests, but keep a watch on all changes.
