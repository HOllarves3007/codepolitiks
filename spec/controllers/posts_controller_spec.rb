RSpec.describe PostsController, type: :controller do

  include Devise::Test::ControllerHelpers

  let(:valid_params){FactoryGirl.attributes_for(:post)}
  let(:invalid_params){FactoryGirl.attributes_for(:post_invalid)}
  let(:test_post){Post.create(valid_params)}

  context "Authenticated user" do
    before do
      user = FactoryGirl.create(:user)
      auth_headers = user.create_new_auth_token
      request.headers.merge!(auth_headers)
    end
    describe "DELETE" do

      DatabaseCleaner.strategy = :truncation
      DatabaseCleaner.start

      describe "/posts" do
        before(:each) do
          test_post = Post.create(valid_params)
          delete :destroy, params:{id:test_post.id}
        end
        it "deletes a single post" do
          json = JSON.parse(response.body)
          expect(json["success"]).to be_truthy
        end
        it " should return 200 status" do
          expect(response.status).to eq 200
        end
      end
    end
    describe "PUT" do

      DatabaseCleaner.strategy = :transaction
      DatabaseCleaner.start

      describe "/posts" do
        it "updates post with valid paramenters" do
          put :update, params:{id:test_post.id, post:valid_params}
          expect(response.status).to eq 200
        end
        it "return an error with invalid params" do
          put :update, params:{id:test_post.id, post:invalid_params}
          expect(response).not_to be_success
          expect(response.status).to eq 422
        end
      end
    end
  end
  context "Unauthenticated user" do
    describe "/posts" do

      before(:each) do
        test_post = Post.create(valid_params)
        delete :destroy, params:{id:test_post.id}
      end

      it "shouldn't delete a single post" do
        json = JSON.parse(response.body)
        expect(json["success"]).to be_falsey
      end
      it " should return 401 status" do
        expect(response.status).to eq 401
      end
    end
  end
  after(:all) do
    DatabaseCleaner.clean
  end
end
