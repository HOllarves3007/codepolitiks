RSpec.describe "Post", type: :request do
  include Devise::Test::IntegrationHelpers

  let(:valid_params){FactoryGirl.attributes_for(:post)}
  let(:invalid_params){FactoryGirl.attributes_for(:post_invalid)}
  let(:test_post){Post.create(valid_params)}

  describe "GET" do
    describe "/posts" do
      it "gets all post" do
        get '/posts'
        expect(response.status).to eq 200
      end
      it "returns success property" do
        get '/posts'
        json = JSON.parse(response.body)
        expect(json['success']).to be_truthy
      end
      it "return 10 posts only" do
        get '/posts'
        expect(response.headers['Per-Page']).to eq '10'
      end
    end
    describe "/posts/:id" do
      it "shows a single post" do
        get '/posts', params:{id:test_post.id}
        expect(response.status).to eq 200
      end
      it "returns success property" do
        get '/posts', params:{id:test_post.id}
        json = JSON.parse(response.body)
        expect(json['success']).to be_truthy
      end
    end
  end
  context "Authenticated user" do
    describe "POST" do

      DatabaseCleaner.strategy = :transaction
      DatabaseCleaner.start

      before(:each) do
        user = FactoryGirl.create(:user)
        @auth_headers = user.create_new_auth_token
      end
      describe "/posts" do
        it "it return 200 status" do
          post '/posts', params:{post:valid_params}, headers: @auth_headers
          expect(response.status).to eq 201
        end
        it "returns an error with invalid params" do
          post '/posts', params:{post:invalid_params}, headers: @auth_headers
          expect(response).not_to be_success
          expect(response.status).to eq 422
        end
      end
    end
  end
  context "Unauthorized user" do
    describe "/posts" do
      it "returns error for unauthorized user" do
        post '/posts', params:{post:valid_params}
        expect(response.status).to eq 401
      end
    end
  end
  after(:all) do
    DatabaseCleaner.clean
  end
end
