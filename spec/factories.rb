require 'faker'

FactoryGirl.define do
  factory :post do
    title { Faker::Book.title }
    body { Faker::Lorem.paragraph(3) }
    factory :post_invalid do
      title ''
      body {Faker::Lorem.paragraph(3) }
    end
  end
  factory :user do
    name {Faker::Name.name }
    nickname {Faker::StarWars.character }
    password "secrets_1"
    password_confirmation "secrets_1"
    image {Faker::Avatar.image }
    email {Faker::Internet.email }
  end
  factory :admin do
    email 'email@email.com'
    password 'fuckthatshit'
  end
end